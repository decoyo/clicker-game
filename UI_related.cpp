#include "Index.h"

void SuccessOperation()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_GREEN);
	printf_s("\n\t\t\tOperation successfully completed!\t\t\t\n\n");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
}

void ErrorOperation(char data[])
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_RED);
	printf_s("\n%s\n", data);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
}

void ChoiceOperation(char data[])
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_BLUE);
	printf_s("\n%s\n", data);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
}
