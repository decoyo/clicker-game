#include <fstream>
#include <iostream>
#include <windows.h>
#include <direct.h>
#include <conio.h>

using namespace std;

enum menuTypes {mainMenu = 0, gameMenu, LoadUser};

struct Session{
	char userName[24]; // @ASKWHY accepts only 22 symbols;
	int points;
	long timeFromLastVisit; // @Todo 
};

//Game related;
void Game(Session);

//Menu related;
int Menus(int);

//UI related;
void SuccessOperation();
void ErrorOperation(char []);
void ChoiceOperation(char []);

//Sessions related;
void CreateSession();
void SaveSessionToFile(Session, bool);
int CountSessions();
Session * LoadAllSessions();
bool CheckIfEmpty (char[]);

//Print related;
void ShowResults();
void SortArray(Session *);