#include "Index.h";

void ShowResults()
{
	system("cls");
	int resAmount = CountSessions();
	Session * datas = new Session [resAmount];
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_BLUE);
	printf_s("\n%24s%10s\n", "USER NAME:", "POINTS:");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
	ifstream fin("AppData/Sessions", std::ios_base::binary | std::ios_base::in);
	for (int i = 0; i < resAmount; i++)
	{
		fin.read((char*)&datas[i], sizeof(Session));
	}
	fin.close();
	SortArray(datas);
	for (int i = 0; i < resAmount; i++)
	{
		printf_s("%24s%10d\n", datas[i].userName, datas[i].points);
	}
	ChoiceOperation("\n   Press any key to continue...   ");
	_getch();
	return;
}

void SortArray(Session * datas) // Sorting via bubbles method;
{
	int size = CountSessions();
	Session temp;
	for(int i = 0; i < size - 1; i++)
	{
		for(int j = 0; j < size - 1 - i; j++)
		{
			if(datas[j].points < datas[j + 1].points)
			{
				temp = datas[j];
				datas[j] = datas[j + 1];
				datas[j + 1] = temp;
			}			
		}		
	}
	return;
}