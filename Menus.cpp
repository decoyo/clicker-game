#include "Index.h"

int Menus(int type)
{
	char ** menu = nullptr;
	int choice = -1, position = 0, menuSize;
	bool selected = false;
	switch(type)
	{
	case menuTypes::mainMenu:
		menuSize = 4;
		menu = new char * [menuSize];
		menu[0] = "Start new game\n";
		menu[1] = "Load and continue game\n";
		menu[2] = "Watch high scores\n";
		menu[3] = "Exit game\n";
		break;
	case menuTypes::gameMenu:
		menuSize = 3;
		menu = new char * [menuSize];
		menu[0] = "Continue game\n";
		menu[1] = "Save game and exit\n";
		menu[2] = "Exit without saving\n";
		break;
	case menuTypes::LoadUser:{
		char numToChar[10];
		menuSize = CountSessions();
		Session t;
		menu = new char * [menuSize];
		Session * temp = LoadAllSessions();
		for (int i = 0; i < menuSize; i++)
		{
			menu[i] = new char [35];
			strcpy(menu[i], temp[i].userName);
			strcat(menu[i], " --> ");
			_itoa(temp[i].points, numToChar, 10); // 10 is measurement system type;
			strcat(menu[i], numToChar);
			strcat(menu[i], "\n");
		}
		delete [] temp;
		}
		break;
	}
	while (true)
	{
		system("cls");
		ChoiceOperation("Please choose option you want to do. Use UP and DOWN arrows to navigate, ENTER to make a choice:\n");
		for (int i = 0; i < menuSize; i++)
		{
			if (i == position)
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_GREEN);
				printf_s(" -> ");
				printf_s("%s", menu[i]);
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
				continue;
			}
			else{
				printf_s("    ");
			}
			printf_s("%s", menu[i]);
		}
		switch (_getch())
		{
		case 224:
			switch (_getch())
			{
			case 80:
				position < menuSize - 1 ? position++ : position = 0;
				break;
				
			case 72:
				position > 0 ? position-- : position = menuSize - 1;
				break;
			}
			break;
		case 13:
			selected = true;
			break;
		}
		if (selected)
		{
			break;
		}
	}
	delete [] menu;
	return position;
}