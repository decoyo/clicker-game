#include "Index.h"

void Game(Session data)
{
	int code = -1, colorChange = 1, pointsLevel = 0, userEntered = -2;
	bool overwrite = true;
	if (data.points == 0)
	{
		overwrite = false;
	}
	// Show ASCI art, change colors;
	while(true)
	{
		system("cls");
		if (colorChange == 9){ // 9 is max color allowed by sytem;
				colorChange = 1;
			}
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), colorChange | 0);
		printf_s("\n\n\n");
		printf_s("\t\t_|    _|  _|    _|          _|                                      _|  \n");
		printf_s("\t\t_|    _|      _|_|_|_|      _|  _|      _|_|    _|    _|    _|_|_|  _|  \n");
		printf_s("\t\t_|_|_|_|  _|    _|          _|_|      _|_|_|_|  _|    _|  _|_|      _|  \n");
		printf_s("\t\t_|    _|  _|    _|          _|  _|    _|        _|    _|      _|_|      \n");
		printf_s("\t\t_|    _|  _|      _|_|      _|    _|    _|_|_|    _|_|_|  _|_|_|    _|  \n");
		printf_s("\t\t                                                      _|                \n");
		printf_s("\t\t                                                  _|_|                  \n");
		printf_s("\n\n\n");
		if (data.points > pointsLevel){
			colorChange++;
			pointsLevel += 100;
		}
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_BLUE);
		printf_s("\tWelcome to the game! %24s \t\t\tYour scores: %5d         ", data.userName, data.points);
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
		// Check key, show error or count points;
		while(true)
		{
			if (_kbhit())
			{
				code = _getch();
				if (code!=224 && code == userEntered){
					ErrorOperation("\tYou are not allowed to click same key two times in a row! Press to continue...  \t\t");
					_getch();
					userEntered = code;
					break;
				}
				userEntered = code;
				switch (code)
				{
				//////////////////
					case 224:
						switch (_getch())
						{
						case 80: case 72: case 75: case 77:
							ErrorOperation("  \t\t\t\t\tArrows are not allowed!  \t\t\t\t\t");
							Sleep(1500);
							break;
						}
						break;
					case 27:
						switch (Menus(menuTypes::gameMenu))
						{
						case 0:
							break;
						case 1: 
							SaveSessionToFile(data, overwrite);			
							return;
						case 2:
							return;
						}
						break;
					default:
						if (data.points <= 100){
							data.points += 10;
							break;
						}
						else if (data.points > 100 && data.points <= 1000){
							data.points += 15;
							break;
						}
						else{
							data.points += 20;
							break;
						}
					}
				break;
				}
			}
	}
}