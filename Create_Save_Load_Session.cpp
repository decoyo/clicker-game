#include "Index.h"

void CreateSession()
{
	Session temp;
	while (true){
		ChoiceOperation("Enter your name:\n");
		gets(temp.userName);
		if (CheckIfEmpty(temp.userName) == true){
			ErrorOperation("\nName of file cannot be empty! Press to continue...\n");
			_getch();
			continue;
		}
		else{
			break;
		}
	}
	if (strlen(temp.userName) > 24){
				temp.userName[23] = '\0';
			}
	temp.points = 0;
	Game(temp);
	return;
}

void SaveSessionToFile(Session temp, bool overwrite)
{
	switch(overwrite){
	case 0:{
		ofstream fout("AppData/Sessions", ios_base::binary | ios_base::app);
		fout.write((char *)&temp, sizeof(Session));
		fout.close();}
		break;
	case 1:{
		int structID = 0;
		int counter = CountSessions();
		Session t;
		fstream file("AppData/Sessions", std::ios_base::binary | std::ios_base::in | std::ios_base::out);
		for (int i = 0; i < counter; i++){
		file.read((char *) &t, sizeof(Session));
		structID++;
		if (strcmp(t.userName, temp.userName) == 0){
			file.seekp((structID - 1) * sizeof(temp), ios::beg);  
			file.write(reinterpret_cast<char *>(&temp),sizeof(temp));  
			file.close();
			return;
		}
		}
		file.close();}
		break;
	}
	SuccessOperation();
	return;
}

int CountSessions()
{
	Session t;
	int  countStructures = 0;
	ifstream fin("AppData/Sessions", std::ios_base::binary | std::ios_base::in);
	while (fin.peek() != EOF){
		fin.read((char*) &t, sizeof(Session));
		countStructures++;
	}
	fin.close();
	return countStructures;
}

Session * LoadAllSessions()
{
	int counter = CountSessions();
	Session * datas = new Session [counter];
	ifstream fin("AppData/Sessions", std::ios_base::binary | std::ios_base::in);
	for (int i = 0; i < counter; i++){
		fin.read((char*) &datas[i], sizeof(Session));
	}
	fin.close();
	return datas;
}

bool CheckIfEmpty (char data[])
{
	if (data[0] == 0)
	{
		return true;
	}
	return false;
}